
using Microsoft.EntityFrameworkCore;
using System;
using chuck_swapi_api.Model;

namespace chuck_swapi_api.Model{
    public class Chuck{
       
        public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
          public  string CreatedUser { get; set; }
         public DateTime? ModifiedDate { get; set; }
         public string ModifiedUser { get; set; }
         public string Category { get; set; }
         public string url { get; set; }

    }
}
