
using Microsoft.EntityFrameworkCore;
using System;
using chuck_swapi_api.Model;

namespace chuck_swapi_api.Model{
    public class StarWar{
      

 public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
          public  string CreatedUser { get; set; }
         public DateTime? ModifiedDate { get; set; }
         public string ModifiedUser { get; set; }
         public string Name { get; set; }
         public string Height { get; set; }
         public string Mass { get; set; }
         public string Hair_color { get; set; }
         public string Skin_color { get; set; }
         public string Eye_color { get; set; }
         public string Birth_year { get; set; }
         public string Gender { get; set; }
         public string Homeworld { get; set; }
         public string url { get; set; }
         public string Code { get; set; }
    }
}
