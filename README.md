# chuck-swapi-api

#Use VSCode as editor

# Install the following:

#Dotnet core 2.2

#Sql server any version

#Sql management studio or Azure studio

#To create db open the sql management studio then click New Query option

#There's a file name scrip.sql inside the project, open the file then copy everything inside the file

#Then go back to sql management studio, paste and run the queries

#Go to the project AppSettings and change the DbConnection to match your connection

#Ensure C# is installed(Intall using Extentions on VSCode)

# To run the API Swagger:

#Navigate to C:\User\chuck-swapi-api and open with CMD

#Enter the following command: dotnet build

#Then enter: dotnet run

#And go to the browser and navigate to: https://localhost:5001/swagger(if you see the Not Secure Warning, press Advance then press Proceed anyway)

#Finally you can use F5 after completing all the instrustions above.

