using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using chuck_swapi_api.Model;
using chuck_swapi_api.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace chuck_swapi_api.Controllers
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public SearchController(DatabaseContext databaseContext, IConfiguration config)
        {
            _config = config;
            _dbcontext = databaseContext;
        }
        [HttpGet]
        [Route("Jokes/{query}")]
        public IActionResult GetJokes(string query)
        {
            string baseUrl = _config.GetSection("MySettings").GetSection("url").Value;
            SqlParameter para = new SqlParameter("@category", "%" + query + "%");
            var data = _dbcontext.Jokes.FromSql($"select * from dbo.Jokes where category LIKE @category", para).ToList();
            ChuckViewModel view = null;
            List<ChuckViewModel> listViews = new List<ChuckViewModel>();
            foreach (var item in data)
            {
                view = new ChuckViewModel
                {
                    Id = item.Id,
                    Category = item.Category,
                    CreatedDate = DateTime.Now,
                    CreatedUser = "Greg",
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = "Greg",
                    url = baseUrl + "Search/Jokes/" + query

                };
                listViews.Add(view);

            }
            return Ok(listViews);
        }
        [HttpGet]
        [Route("People/{query}")]
        public IActionResult GetPeople(string query)
        {
            string baseUrl = _config.GetSection("MySettings").GetSection("url").Value;
            SqlParameter para = new SqlParameter("@name", "%" + query + "%");
            var data = _dbcontext.People.FromSql($"select * from dbo.People where name LIKE @name", para).ToList();
            StarWarViewModel view = null;
            List<StarWarViewModel> listViews = new List<StarWarViewModel>();


            var films = new List<string>();
            var vehicles = new List<string>();
            var starships = new List<string>();

            films.Add(baseUrl + "/Swapi/Film/1");
            films.Add(baseUrl + "/Swapi/Film/2");
            films.Add(baseUrl + "/Swapi/Film/3");

            vehicles.Add(baseUrl + "/Swapi/Vehicle/1");
            vehicles.Add(baseUrl + "/Swapi/Vehicle/2");
            vehicles.Add(baseUrl + "/Swapi/Vehicle/3");

            starships.Add(baseUrl + "/Swapi/Starship/1");
            starships.Add(baseUrl + "/Swapi/Starship/2");
            starships.Add(baseUrl + "/Swapi/Starship/3");




            foreach (var item in data)
            {
                view = new StarWarViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Height = item.Height,
                    Mass = item.Mass,
                    Hair_color = item.Hair_color,
                    Skin_color = item.Skin_color,
                    Eye_color = item.Eye_color,
                    Birth_year = item.Birth_year,
                    Gender = item.Gender,
                    Homeworld = baseUrl + "Search/People/"+ query,
                    CreatedDate = DateTime.Now,
                    CreatedUser = "Greg",
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = "Greg",
                    url = baseUrl + "Search/People/"+ query,
                    Vehicles = vehicles,
                    Films = films,
                    Starships = starships

                };
                listViews.Add(view);

            }
            return Ok(listViews);
        }

    }
}
