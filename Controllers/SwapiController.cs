using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chuck_swapi_api.Model;
using chuck_swapi_api.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace chuck_swapi_api.Controllers
{
     [Route("api/[controller]")]
    public class SwapiController : Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public SwapiController(DatabaseContext databaseContext,IConfiguration config)
        {
            _config = config;
            _dbcontext = databaseContext;
        }
        [HttpGet]
        [Route("People")]

        public IActionResult GetPeople()
        {
             string baseUrl = _config.GetSection("MySettings").GetSection("url").Value;

            var films = new List<string>();
            var vehicles = new  List<string>();
            var starships = new  List<string>();

            films.Add(baseUrl + "/Swapi/Film/1");
            films.Add(baseUrl + "/Swapi/Film/2");
            films.Add(baseUrl + "/Swapi/Film/3");

            vehicles.Add(baseUrl + "/Swapi/Vehicle/1");
            vehicles.Add(baseUrl + "/Swapi/Vehicle/2");
            vehicles.Add(baseUrl + "/Swapi/Vehicle/3");

            starships.Add(baseUrl + "/Swapi/Starship/1");
            starships.Add(baseUrl + "/Swapi/Starship/2");
            starships.Add(baseUrl + "/Swapi/Starship/3");

            var query = (from p in _dbcontext.People
                         select new StarWarViewModel
                         {
                             Id = p.Id,
                             Name = p.Name,
                             Height = p.Height,
                             Mass = p.Mass,
                             Hair_color = p.Hair_color,
                             Skin_color = p.Skin_color,
                             Eye_color = p.Eye_color,
                             Birth_year = p.Birth_year,
                             Gender = p.Gender,
                             Homeworld = baseUrl + "/Swapi/People",
                             CreatedDate = DateTime.Now,
                             CreatedUser = "Greg",
                             ModifiedDate = DateTime.Now,
                             ModifiedUser  = "Greg",
                             url = baseUrl + "Swapi/People",
                             Vehicles = vehicles,
                             Films = films,
                             Starships = starships
                         }).ToList();
            return Ok(query);
        }
        
    }
}
