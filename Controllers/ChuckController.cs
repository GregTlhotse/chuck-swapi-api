using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chuck_swapi_api.Model;
using chuck_swapi_api.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace chuck_swapi_api.Controllers
{
     [Route("api/[controller]")]
    public class ChuckController : Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public ChuckController(DatabaseContext databaseContext,IConfiguration config)
        {
            _config = config;
            _dbcontext = databaseContext;
        }
        [HttpGet]
        [Route("Categories")]
        public IActionResult GetCategory()
        {
            string baseUrl = _config.GetSection("MySettings").GetSection("url").Value;
            var query = (from p in _dbcontext.Jokes
                         select new ChuckViewModel
                         {
                             Id = p.Id,
                             Category = p.Category,
                             CreatedDate = DateTime.Now,
                             CreatedUser = "Greg",
                             ModifiedDate = DateTime.Now,
                             ModifiedUser  = "Greg",
                             url = baseUrl + "Chuck/Categories"
                         }).ToList();
            return Ok(query);
        }
        [HttpGet]
        [Route("Jokes/Random")]
        public IActionResult GetRandomJoke()
        {
            string baseUrl = _config.GetSection("MySettings").GetSection("url").Value;
            var query = (from p in _dbcontext.Jokes
                         where p.Category != null
                         select new ChuckViewModel
                         {
                             Id = p.Id,
                             Category = p.Category,
                             CreatedDate = DateTime.Now,
                             CreatedUser = "Greg",
                             ModifiedDate = DateTime.Now,
                             ModifiedUser  = "Greg",
                             url = baseUrl + "Jokes/Random/" + p.Category
                         }).OrderBy(c => Guid.NewGuid())
                            .FirstOrDefault();
            return Ok(query);
        }
        
    }
}
