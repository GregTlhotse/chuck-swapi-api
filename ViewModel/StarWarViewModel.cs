
using Microsoft.EntityFrameworkCore;
using System;
using chuck_swapi_api.ViewModel;
using System.Collections.Generic;

namespace chuck_swapi_api.ViewModel{
    public class StarWarViewModel{
         public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
          public  string CreatedUser { get; set; }
         public DateTime? ModifiedDate { get; set; }
         public string ModifiedUser { get; set; }
         public string Name { get; set; }
         public string Height { get; set; }
         public string Mass { get; set; }
         public string Hair_color { get; set; }
         public string Skin_color { get; set; }
         public string Eye_color { get; set; }
         public string Birth_year { get; set; }
         public string Gender { get; set; }
         public string Homeworld { get; set; }
         public string url { get; set; }
         public List<string> Films { get; set; }
         public List<string> Species { get; set; }
         public List<string> Vehicles { get; set; }
         public List<string> Starships { get; set; }
         
    }
}
