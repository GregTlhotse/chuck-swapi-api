
using Microsoft.EntityFrameworkCore;
using System;
using chuck_swapi_api.ViewModel;

namespace chuck_swapi_api.ViewModel{
    public class ChuckViewModel{
       
        public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
          public  string CreatedUser { get; set; }
         public DateTime? ModifiedDate { get; set; }
         public string ModifiedUser { get; set; }
         public string Category { get; set; }
         public string url { get; set; }

    }
}
