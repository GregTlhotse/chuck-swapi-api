CREATE DATABASE ChuckStarWar;
go
USE ChuckStarWar;
go
CREATE TABLE Jokes (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    Category varchar(255),
    url varchar(255),
);
go
CREATE TABLE People (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    Name varchar(255),
    Height varchar(255),
    Mass varchar(255),
    Hair_color varchar(255),
    Skin_color varchar(255),
    Eye_color varchar(255),
    Birth_year varchar(255),
    Gender varchar(255),
    Homeworld varchar(255),
    url varchar(255),
    Code varchar(255)
);
go
INSERT INTO Jokes (Category)
VALUES ('Money');
go
INSERT INTO Jokes (Category)
VALUES ('Animal');
go
INSERT INTO Jokes (Category)
VALUES ('Celebrity');
go
INSERT INTO Jokes (Category)
VALUES ('Fashion');
go
INSERT INTO Jokes (Category)
VALUES ('Movie');
go
INSERT INTO People (Name,Height,Mass,Hair_color,Skin_color,Eye_color,Birth_year,Gender,Code)
VALUES ('Luke Skywalker','167','552','Brown','Dark','Green','2020','Male','1');
go
INSERT INTO People (Name,Height,Mass,Hair_color,Skin_color,Eye_color,Birth_year,Gender,Code)
VALUES ('C-3PO','167','552','Brown','Dark','Green','2020','Male','2');
go
INSERT INTO People (Name,Height,Mass,Hair_color,Skin_color,Eye_color,Birth_year,Gender,Code)
VALUES ('R2-D2','167','552','Brown','Dark','Green','2020','Male','3');
go
INSERT INTO People (Name,Height,Mass,Hair_color,Skin_color,Eye_color,Birth_year,Gender,Code)
VALUES ('Leia Organa','167','552','Light Brown','Dark','Green','2020','Male','4');
go
INSERT INTO People (Name,Height,Mass,Hair_color,Skin_color,Eye_color,Birth_year,Gender,Code)
VALUES ('Owen Lars','167','552','Light Brown','Dark','Green','2020','Male','5');